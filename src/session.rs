//use std::collections::HashMap;
use regex::Regex;
//use reqwest::header;

use cookie_store::CookieStore;
use std::io::Read;
//use std::fs::File;
//use std::io::BufReader;

//use std::{thread, time};
//use std::ops::Sub;
use sha2::Digest;
//use serde_json::Map;
#[derive(Debug, Clone)]
pub enum StreamError {
    NotLiveContent(String),
    NotLive(String),
    ParsingError(String),
}
/*pub struct SessionOptions {
    hls_live_edge: u32,
    hls_segment_attempts: u32,
    hls_segment_ignore_names: Vec<String>,
    hls_segment_threads: u8,
    hls_segment_timeout: u16,
    hls_segment_stream_data: bool,
    hls_timeout: u16,
    hls_playlist_reload_attempts: u8,
    hls_playlist_reload_time: String,
    hls_start_offset: u16,
    hls_duration: u64,
    http_stream_timeout: u16,
    ringbuffer_size: u64,
    stream_segment_attempts: u16,
    stream_segment_threads: u8,
    stream_segment_timeout: u16,
    stream_timeout: u16,
}*/
pub struct Session {
    pub http: ureq::Agent, //reqwest::blocking::Client,
    //jsonhttp: reqwest::blocking::Client,
    //quality_order: Vec<String>,
    //options: SessionOptions,
    url: String,
    videoid: String,
    //prev_links: Vec<String>,
    //regex_initial_data: Regex,
    regex_initial_player_response: Regex,
    //regex_mime_type: Regex,
    regex_i_api_key: Regex,
    regex_i_version: Regex,
    //media_seq: String,
    //first_read: bool,
    //first_m3u8: bool,
    last_hashes: Vec<String>,
    //three_seconds: time::Duration,
    //extra: u32,
}
impl Session {
    pub fn new(url: String) -> Self {
        //let quality_order: Vec<String> = vec!["1080p60".to_string(), "1080p".to_string(), "720p60".to_string(), "720p".to_string()];
        let url_regex = Regex::new(r"^.*(?:(?:youtu\.be/|v/|vi/|u/w/|embed/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*").unwrap();
        //let mut videoid = "DEFAULT".to_string();
        let videoid: String = url_regex
            .captures(&url)
            .expect("Not a valid YouTube url")
            .get(1)
            .unwrap()
            .as_str()
            .to_string();
        let newurl: String = format!("https://www.youtube.com/watch?v={}", videoid.clone());
        let cookies = crate::parse_cookie_file("cookies.txt");
        //println!("{:?}", cookies);
        //let mut headers = header::HeaderMap::new();
        //let mut cookiestring = String::new();
        //let mut media_seq = ":1".to_string();
        //let mut first_read = true;
        //let mut first_m3u8 = true;
        //let mut prev_links: Vec<String> = Vec::new();
        //cookie_store
        let hashes: Vec<String> = Vec::new();
        //for (key, value) in cookies.into_iter() {
        //    cookiestring.push_str(format!("{}={}; ", key.to_string(), value.to_string()).as_str());
        //}
        //println!("{}", cookiestring);
        //headers.insert("Cookie", header::HeaderValue::from_str(cookiestring.as_str()).unwrap());
        //let http = reqwest::blocking::Client::builder()
        //    .user_agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36")
        //    .default_headers(headers)
        //    .build()
        //    .unwrap();
        //let mut newheaders = header::HeaderMap::new();
        //newheaders.insert("Cookie", header::HeaderValue::from_str(cookiestring.as_str()).unwrap());
        //newheaders.insert("Content-Type", header::HeaderValue::from_str("application/json").unwrap());
        //let jsonhttp = reqwest::blocking::Client::builder()
        //    .user_agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36")
        //    .default_headers(newheaders)
        //    .build()
        //    .unwrap();
        //let mut extra = 3;
        //let file = File::open("cookies.json").unwrap();
        //let mut read = BufReader::new(file);
        //let mut buf = String::new();
        //read.read_to_string(&mut buf).unwrap();
        //println!("{}", buf);
        // Read persisted cookies from cookies.json
        let mut my_store = CookieStore::load_json("".as_bytes()).unwrap();
        for (key, value) in cookies.clone().into_iter() {
            my_store
                .insert_raw(
                    &cookie::Cookie::new(key, value),
                    &url::Url::parse("https://www.youtube.com").unwrap(),
                )
                .unwrap();
        }
        for (key, value) in cookies.clone().into_iter() {
            my_store
                .insert_raw(
                    &cookie::Cookie::new(key, value),
                    &url::Url::parse("https://youtube.com").unwrap(),
                )
                .unwrap();
        }
        // Cookies will be used for requests done through agent.
        let agent = ureq::builder()
            .cookie_store(my_store)
            .user_agent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36")
            .build();
        Self {
            http: agent,
            //jsonhttp: jsonhttp,
            //quality_order: quality_order,
            //options: options,
            url: newurl,
            videoid: videoid,
            //regex_initial_data: Regex::new(r#"(?s)var\s+ytInitialData\s*=\s*(\{.*?\})\s*;\s*</script>"#).unwrap(),
            regex_initial_player_response: Regex::new(
                r#"(?s)var\s+ytInitialPlayerResponse\s*=\s*(\{.*?\});\s*var\s+meta\s*="#,
            )
            .unwrap(),
            //regex_mime_type: Regex::new(r#"^(?P<type>\w+)/(?P<container>\w+); codecs="(?P<codecs>.+)"$"#).unwrap(),
            regex_i_api_key: Regex::new(r#""INNERTUBE_API_KEY":\s*"([^"]+)""#).unwrap(),
            regex_i_version: Regex::new(r#""INNERTUBE_CLIENT_VERSION":\s*"([\d\.]+)""#).unwrap(),
            //media_seq: media_seq,
            //first_read: first_read,
            //first_m3u8: first_m3u8,
            //prev_links: prev_links,
            last_hashes: hashes,
            //three_seconds: time::Duration::new(3, 0),
            //extra: extra,
        }
    }
    pub fn read(&mut self, url: String) -> Result<Vec<u8>, std::io::Error> {
        let mut outdata: Vec<u8> = Vec::new();
        let req = self.http.get(url.as_str()).call();
        if req.is_err() {
            return Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "Stream ended",
            ));
        }
        let bestdata = req.unwrap().into_string().unwrap();
        //let mut startinst = time::Instant::now();
        //let mut indexurl: String = String::new();
        //for line in data.lines() {
        //    if line.to_string() != String::new() {
        //        indexurl = line.to_string();
        //    }
        //}
        //let bestdata = self.http.get(indexurl.as_str()).send().unwrap().text().unwrap();
        let mut tslinks: Vec<String> = Vec::new();
        //let mut timestamp = String::new();
        //let mut diff: u32 = 0;
        for line in bestdata.lines() {
            if line.ends_with("seg.ts") {
                tslinks.push(line.to_string());
            }
        }
        /*
            else if line.starts_with("#EXT-X-MEDIA-SEQUENCE:") {
                if self.first_read {
                    self.media_seq = line.to_string();
                    self.first_read = false;
                    //self.extra -= 1;
                }
                let media_seq = line.to_string();
                let mut ms1 = media_seq.split(":").last().unwrap().parse::<u32>().unwrap();
                let mut ms2 = self.media_seq.split(":").last().unwrap().parse::<u32>().unwrap();
                //println!("{}", media_seq);
                diff = ms1 - ms2;
                //println!("{}", diff);
                if diff == 0 && !self.first_m3u8 {
                    return Ok(outdata);
                }
                if diff > 3 {
                    diff = 3;
                }
                //if self.extra > 0 && diff > 0 {
                //    self.extra -= diff;
                //}
                self.media_seq = line.to_string();
                //if diff < 3 {
                //    return Ok(outdata);
                //}
                //self.media_seq = line.to_string();
                //let diff =  media_seq.split(":").last().unwrap().parse::<u32>().unwrap() - self.media_seq.split(":").last().unwrap().parse::<u32>().unwrap() < 3 {
                //    return Ok(outdata);
                //} else {
                //    self.media_seq = line.to_string();
                //}
            }
        }
        self.extra = 0;
        let mut startagain = true;
        let mut startnexttime = true;
        for (num, tslink) in tslinks.clone().into_iter().enumerate() {
            let mut skip = false;
            if (diff as usize) >  num || self.first_m3u8  {
                //println!("num = {}", num);
                let mut buffer: Vec<u8> = Vec::new();
                self.http.get(tslink.as_str()).send().unwrap().read_to_end(&mut buffer)?;
                outdata.append(&mut buffer);
                //break;
            }
        }
        self.first_m3u8 = false;*/
        for tslink in tslinks.clone() {
            let mut buffer: Vec<u8> = Vec::new();
            match self.http.get(tslink.as_str()).call() {
                Ok(x) => {
                    x.into_reader().read_to_end(&mut buffer)?;
                }
                Err(_x) => {
                    continue;
                }
            }
            //match &mut self.http.get(tslink.as_str()).call().unwrap().into_reader() {
            //    Ok(h) => {
            //        h.read_to_end(&mut buffer)?;
            //    },
            //    Err(_) => {
            //        return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, "Stream ended"));
            //    }
            //} //.read_to_end(&mut buffer)?;
            let seghash = format!("{:x}", sha2::Sha512::digest(buffer.clone().as_slice()));
            if !self.last_hashes.contains(&seghash.clone()) {
                outdata.append(&mut buffer.clone());
                self.last_hashes.push(seghash.clone());
            } //else {
            if tslinks.clone().len() < self.last_hashes.len() {
                self.last_hashes.remove(0);
            }
        }
        //println!("{}", self.last_hashes.len());
        if !outdata.is_empty() || !self.sessions().is_err() {
            Ok(outdata)
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "Stream ended",
            ))
        }
    }

    /*fn get_response(&mut self, response_data: String, re: Regex) -> serde_json::Value {
        //let response = self.http.get(&url).send().unwrap();
        //let response_data = response.text().unwrap();
        let ytresponse = self.get_data_from_regex(response_data, re.clone(), String::new());
        ytresponse
    }*/
    pub fn sessions(&mut self) -> Result<String, StreamError> {
        let response = self.http.get(&self.url).call().unwrap();
        let response_data = response.into_string().unwrap();
        let resp = self.get_data_from_regex(
            response_data.clone(),
            self.regex_initial_player_response.clone(),
        );
        let mut json_value: serde_json::Value;
        if resp.is_some() {
            json_value = resp.unwrap();
        } else {
            let i_api_key = match self.regex_i_api_key.captures(response_data.as_str()) {
                Some(z) => match z.get(1) {
                    Some(a) => a.as_str().to_string(),
                    None => "AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8".to_string(),
                },
                None => "AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8".to_string(),
            };
            let i_version = match self.regex_i_version.captures(response_data.as_str()) {
                Some(z) => match z.get(1) {
                    Some(a) => a.as_str().to_string(),
                    None => "1.20210616.1.0".to_string(),
                },
                None => "1.20210616.1.0".to_string(),
            };
            //.unwrap().get(1).unwrap().as_str().to_string();
            //let i_api_key = "AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8".to_string();
            //let i_version = self.regex_i_version.captures(response_data.as_str()).unwrap().get(1).unwrap().as_str().to_string();
            //let i_version = "1.20210616.1.0".to_string();
            //println!("{} {}", i_api_key, i_version);
            let outjson = ureq::json!({
                "videoId": self.videoid,
                "contentCheckOk": true,
                "racyCheckOk": true,
                "context": {
                    "client": {
                        "clientName": "WEB",
                        "clientVersion": i_version,
                        "platform": "DESKTOP",
                        "clientScreen": "EMBED",
                        "clientFormFactor": "UNKNOWN_FORM_FACTOR",
                        "browserName": "Chrome",
                    },
                    "user": {"lockedSafetyMode": "false"},
                    "request": {"useSsl": "true"},
                }
            });
            let tempres = self
                .http
                .post("https://www.youtube.com/youtubei/v1/player")
                .query("key", i_api_key.as_str())
                .send_json(outjson)
                .unwrap();
            //println!("{:?}", tempres.get_url());
            //let res = self.http.post(format!("https://www.youtube.com/youtubei/v1/player?key={}", i_api_key).as_str())
            //    .send_json(outjson).unwrap().into_string().unwrap();
            let res = tempres.into_string().unwrap();
            //let res = self.http.post(format!("http://127.0.0.1:6669/youtubei/v1/player?key={}", i_api_key).as_str())
            //    .send_json(outjson).unwrap().into_string().unwrap();
            json_value = serde_json::from_str(res.as_str()).unwrap();
        }
        //println!("Initial Response: {:?}", self.get_response(response_data.as_str().to_string(), self.regex_initial_data.clone()));
        //println!("1");
        //println!("Initial Player Response: {:?}", self.get_response(response_data.as_str().to_string(), self.regex_initial_player_response.clone()));
        //println!("2");
        let jsonmap = json_value.as_object_mut().unwrap();
        if !&jsonmap.contains_key("videoDetails") {
            return Err(StreamError::ParsingError(format!(
                "No such key 'videoDetails'"
            )));
        }
        let video_details = &jsonmap["videoDetails"].as_object_mut().unwrap();
        let is_live: bool;
        if video_details.contains_key("isLiveContent") {
            if video_details["isLiveContent"].as_bool().unwrap() {
                if !video_details.contains_key("isLive") {
                    return Err(StreamError::NotLive(
                        "This livestream is not live yet.".to_string(),
                    ));
                }
                if video_details["isLive"].as_bool().unwrap() {
                    is_live = true;
                } else {
                    return Err(StreamError::NotLive(
                        "This livestream is not live yet.".to_string(),
                    ));
                }
            } else {
                return Err(StreamError::NotLiveContent(
                    "This is not a livestream.".to_string(),
                ));
            }
        } else {
            return Err(StreamError::NotLiveContent(
                "This is not a livestream.".to_string(),
            ));
        }
        let streams: String;
        let mut bestdata = String::new();
        //is_live = true;
        //let mut streams: HashMap<String, String> = HashMap::new();
        if is_live {
            //println!("{}", &jsonmap["streamingData"].to_string());
            /*let mut first = true;
            //println!("{}", &jsonmap["streamingData"].to_string());
            let streamingdata = &jsonmap["streamingData"].as_object_mut().unwrap();
            if streamingdata.contains_key("adaptiveFormats") {//&& streamingdata.contains_key("hlsManifestUrl") {
                let adaptive_formats = &jsonmap["streamingData"]["adaptiveFormats"].as_array().unwrap().to_vec();
                //let mut afvec = adaptive_formats.to_vec();
                for adaptive_format in adaptive_formats {
                    //println!("{}", adaptive_format.to_string());
                    if adaptive_format["qualityLabel"] != serde_json::Value::Null {
                        //println!("'{}'", adaptive_format["qualityLabel"].as_str().unwrap().to_string());
                        if first {
                            streams.insert("best".to_string(), adaptive_format["url"].as_str().unwrap().to_string());
                            first = false;
                        }
                        streams.insert(adaptive_format["qualityLabel"].as_str().unwrap().to_string(), adaptive_format["url"].as_str().unwrap().to_string());
                    }
                }
                //let available_qualities: Vec<&String> = streams.keys().collect::<Vec<&String>>();
                //streams.insert("best".to_string(), streams]);
            }*/
            //println!("{:?}", &jsonmap);i
            if !jsonmap.contains_key("streamingData") {
                return Err(StreamError::ParsingError(
                    "Not longer streaming?".to_string(),
                ));
            } else {
                if !jsonmap["streamingData"]
                    .as_object()
                    .unwrap()
                    .contains_key("hlsManifestUrl")
                {
                    return Err(StreamError::ParsingError(
                        "Not longer streaming?".to_string(),
                    ));
                }
            }
            streams = (&jsonmap)["streamingData"]["hlsManifestUrl"]
                .as_str()
                .unwrap()
                .to_string();
            let data = self
                .http
                .get(streams.as_str())
                .call()
                .unwrap()
                .into_string()
                .unwrap();
            let mut indexurl: String = String::new();
            for line in data.lines() {
                if line.to_string() != String::new() {
                    indexurl = line.to_string();
                }
            }
            bestdata = indexurl;
        }
        Ok(bestdata)
        //println!("jsonvalue: {}", jsonmap["videoDetails"].to_string());
    }
    fn get_data_from_regex(&self, response_data: String, re: Regex) -> Option<serde_json::Value> {
        //Ok(serde_json::from_str(re.captures(response_data.as_str())?.get(1)?.as_str()).unwrap())
        let a = re.captures(response_data.as_str());
        if a.is_none() {
            return None;
        } else {
            let b = a.unwrap();
            let c = b.get(1);
            if c.is_some() {
                let d = c.unwrap().as_str();
                let e = serde_json::from_str(d);
                if e.is_ok() {
                    return Some(e.unwrap());
                }
            }
        }
        return None;
    }
}
