use regex::Regex;
use std::collections::HashMap;
use std::fs;
pub mod session;
pub fn parse_cookie_file<'a>(cookiefile: &str) -> HashMap<String, String> {
    let mut cookies: HashMap<String, String> = HashMap::new();
    let mut cookiefiledata = fs::read_to_string(cookiefile).unwrap();
    cookiefiledata = cookiefiledata.replace("\r", "");
    let re = Regex::new(r"^\#").unwrap();
    for line in cookiefiledata.lines() {
        if !re.is_match(line) && line != "" {
            let linefields = line.trim().split("\t").collect::<Vec<&str>>();
            cookies.insert(linefields[5].to_string(), linefields[6].to_string());
        }
    }
    cookies
}
